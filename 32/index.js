var express = require('express');
var app = express();
var dados;

app.use(express.urlencoded());

app.get('/', function(req, res, next) {
	res.sendFile("public/login.html", { root : __dirname});
})

app.post('/login', function(req, res, next) {
	dados = req.body;
	if(dados.user === '' && dados.password === ''){
		res.sendFile("public/sucesso.html", { root : __dirname});
	}else{
		res.sendStatus(401);
	}
})

app.use(function(req, res){
       res.sendFile("public/404.html", { root : __dirname});
   });


app.listen(80, function () {
	console.log('Aplicação exemplo escutando na porta 80!');
});
