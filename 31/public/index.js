var addCar = document.getElementById("addCar");
var send = document.getElementById("send");
var car = document.getElementById("car");
var listItem = []
var div;
var nome;
var valor;

function createElement(){
	div = document.createElement("div"); 
	nome = document.createElement("div"); 
	valor = document.createElement("div");
	div.className = "w-100";
	nome.className = "col border";
	valor.className = "col border";
}

function atualizaCar(lista){
	lista.forEach(function (element, index, array) {
		nome.textContent = "" + element.nome;
		valor.textContent = "" + element.valor;
		car.appendChild(div);
		car.appendChild(nome);
		car.appendChild(valor);
	});
	
}
	
addCar.addEventListener("click", function(){
	campoNome = document.getElementById("nome")
	campoValor = document.getElementById("valor")
	item = { 
		"nome" : "" + campoNome.value,
		"valor" : "" + campoValor.value
	}
	campoNome.value = '';
	campoValor.value = '';
	listItem.push(item);
	createElement();	
	atualizaCar(listItem);
});

send.addEventListener("click", function(){
	console.log(listItem);
	
	var xhr = new XMLHttpRequest();
	var url = "/add?data=" + encodeURIComponent(JSON.stringify(listItem));
	xhr.open("GET", url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send();
});